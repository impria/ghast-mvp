---
layout: default
---
<section class="hero hero-md">
  {% include navigation.html %}
  <div class="hero-content">
    <div class="container">
      <h1 class="title">
        Ghast
      </h1>
      <h2 class="subtitle description">
        A terrifyingly simple CSS framework
      </h2>
      <p class="hero-button">
        <a href="#getting-started" class="btn">Download</a>
      </p>
    </div>
  </div>
</section>


<section class="container docs-section" id="why">
  <h1 class="title">Is Ghast for you?</h1>
  <p>You should use Ghast if you want a highly customizable base set of features you can expand upon. Ghast was designed to be small and concise and provide sane defaults. You could also say it's a slightly opiniated framework as it relies on flexbox support and thus ditches older browsers (I'm looking at you, IE 8) in favour of newer ones.</p>
  <br>
  <p>Using Ghast? Loving it? We think it'd be super cool if you shared it with your peeps! &hearts;</p>
  <div class="btn-group btn-group-block">
    <a href="https://github.com/impria/ghast" title="Star Ghast on GitHub" target="blank" class="btn"><i class="fa fa-github"></i></a>
    <a href="https://www.facebook.com/sharer/sharer.php?u=https://github.com/impria/ghast" title="Share Ghast on Facebook" target="blank" class="btn"><i class="fa fa-facebook"></i></a>
    <a href="https://twitter.com/intent/tweet?text=Ghast %7C A terrifyingly simple CSS framework&amp;url=https://github.com/impria/ghast" title="Share Ghast on Twitter" target="blank" class="btn"><i class="fa fa-twitter"></i></a>
    <a href="https://plus.google.com/share?url=https://github.com/impria/ghast" title="Share Ghast on Google+" target="blank" class="btn"><i class="fa fa-google"></i></a>
    <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://github.com/impria/ghast&amp;title=Ghast | A terrifyingly simple CSS framework" title="Share Ghast on LinkedIn" target="blank" class="btn"><i class="fa fa-linkedin"></i></a>
  </div>
</section>

<section class="container docs-section" id="getting-started">
  <h1 class="title">Getting Started</h1>
  <p>There are a few ways to get started with Ghast. First, check out all the download options available below, then choose the most suitable option for your needs. Next, just add ghast.min.css and normalize.css in the header of your project.</p>
  <div class="example">
    <div class="row">
      <div class="col">
        <h5>Compiled</h5>
        <p>Compiled and minified source code with source maps. Does not include development tools.</p>
        <a href="#!" class="btn btn-outline btn-block">Download Compiled</a>
      </div>
      <div class="col">
        <h5>Source Code</h5>
        <p>This includes all the source files and compiled files together with <a href="http://gulpjs.com/" title="Learn more about Gulp" target="_blank">Gulp</a> for easy building.</p>
        <a href="#!" class="btn btn-outline btn-block">Download Source</a>
      </div>
      <div class="col">
        <h5>Packaged</h5>
        <p>You can also get Ghast from npm and Bower. This is the equivalent of source code with dev tools.</p>
        <div class="btn-group btn-group-block">
          <a href="#!" class="btn btn-outline">npm</a>
          <a href="#!" class="btn btn-outline">Bower</a>
        </div>
      </div>
    </div>
    <p><strong>What's included</strong></p>
    <p>Once downloaded, extract the compressed folder to see the main file in the uncompressed and minified version.</p>
    <p><pre class="code prettyprint lang-md"><code class="code-content">ghast/
├── app/
│   ├── index.html
├── css/
│   ├── ghast.css
│   ├── ghast.min.css
│   └── ghast.min.css.map
├── gulpfile.js
├── license
└── readme.md
</code></pre></p>
  </div>
</section>

<section class="container docs-section" id="manualinstall">
  <h1 class="title">Compile from source</h1>
  <p>If you need a bit more control over how Ghast looks and behaves, you can also compile your own version from source by following these steps. Ghast uses <a href="https://gulpjs.com/">Gulp</a> to compile CSS, so make sure you have it installed properly for your system!</p>
  <div class="example">
    <p><strong>Grab the source from GitHub</strong></p>
    <p>Clone the repository to a directory of your choosing.</p>
    <p><pre class="code prettyprint"><code class="code-content">$ git clone https://github.com/impria/ghast.git</code></pre></p>
    <p><strong>Install the dependencies</strong></p>
    <p>NPM will install all the required dependencies for you as listed in the <code>package.json</code> file.</p>
    <p><pre class="code prettyprint"><code class="code-content">$ npm install</code></pre></p>
    <p><strong>Compile with Gulp</strong></p>
    <p>Once all the dependencies are installed, run the build task to compile the CSS.</p>
    <p><pre class="code prettyprint"><code class="code-content">$ gulp build</code></pre></p>
    <p><strong>That's it!</strong></p>
    <p>Your compiled files will be located in the <code>/css</code> directory</p>
  </div>
</section>

<section class="container docs-section" id="typography">
  <h1 class="title">Typography</h1>
  <p>CSS3 introduces a few new units, including the <code>rem</code> unit, which stands for <em>"root em"</em>. The <code>rem</code> unit is relative to the font-size of the root element <code>&lt;html&gt;</code>. That means that we can define a single font size on the root element, and define all <code>rem</code> units to be a percentage of that. The typography has <code>font-size</code> defined as 1.6rem (16px) and <code>line-height</code> as 1.65 (26.4px). <strong>Ghast</strong> uses system fonts by default to provide a uniform and integrated look.</p>
  <div class="example">
    <div class="row">
      <div class="col">
        <h1>Heading<span class="heading-font-size"> <code>&lt;h1&gt;</code> 5.0rem (50px)</span></h1>
        <h2>Heading<span class="heading-font-size"> <code>&lt;h2&gt;</code> 4.2rem (42px)</span></h2>
        <h3>Heading<span class="heading-font-size"> <code>&lt;h3&gt;</code> 3.6rem (36px)</span></h3>
        <h4>Heading<span class="heading-font-size"> <code>&lt;h4&gt;</code> 3.0rem (30px)</span></h4>
        <h5>Heading<span class="heading-font-size"> <code>&lt;h5&gt;</code> 2.4rem (24px)</span></h5>
        <h6>Heading<span class="heading-font-size"> <code>&lt;h6&gt;</code> 1.5rem (15px)</span></h6>
      </div>
    </div>
  </div>
  <pre class="code prettyprint lang-html"><code class="code-content">&lt;!-- Base font-size and line-height --&gt;
&lt;p&gt;The base type is 1.6rem (16px) over 1.65 line height (26.4px)&lt;/p&gt;

&lt;!-- Other elements to text markup --&gt;
&lt;a&gt;Anchor&lt;/a&gt;
&lt;em&gt;Emphasis&lt;/em&gt;
&lt;small&gt;Small&lt;/small&gt;
&lt;strong&gt;Strong&lt;/strong&gt;
&lt;u&gt;Underline&lt;/u&gt;

&lt;!-- Default Headings --&gt;
&lt;h1&gt;Heading&lt;/h1&gt;
&lt;h2&gt;Heading&lt;/h2&gt;
&lt;h3&gt;Heading&lt;/h3&gt;
&lt;h4&gt;Heading&lt;/h4&gt;
&lt;h5&gt;Heading&lt;/h5&gt;
&lt;h6&gt;Heading&lt;/h6&gt;

&lt;!-- The base font-size is set at 62.5% for having the convenience of sizing rems in a way that is similar to using px. So basically 1.6rem = 16px. --&gt;</code></pre>
</section>

<section class="container docs-section" id="blockquotes">
  <h1 class="title">Blockquotes</h1>
  <p>The Blockquote represents a section that is quoted from another source.</p>
  <div class="example">
    <blockquote>
      <p><em>“Design is all about finding solutions within constraints; if there were no constraints, it’s not design - it’s art.” — Matias Duarte</em></p>
    </blockquote>
  </div>
  <pre class="code prettyprint"><code class="code-content">&lt;blockquote&gt;
&lt;p&gt;&lt;em&gt;&ldquo;Design is all about finding solutions within constraints; if there were no constraints, it&rsquo;s not design - it&rsquo;s art.&rdquo; &mdash; Matias Duarte&lt;/em&gt;&lt;/p&gt;
&lt;/blockquote&gt;</code></pre>
</section>

<section class="container docs-section" id="buttons">
  <h1 class="title">Buttons</h1>
  <p>The button, an essential part of any user experience. Buttons come in three basic styles in Ghast: The <code>&lt;btn&gt;</code> element has a flat color by default, whereas <code>.btn-outline</code> has a simple outline around, and <code>.btn-clear</code> is entirely clear.</p>
  <div class="col example">
    <a class="btn tooltip" href="#!" data-tooltip="Hey this is a test!" data-tooltip-pos="top">Default btn</a>
    <button class="btn btn-outline">Outline btn</button>
    <input class="btn btn-clear" type="submit" value="Clear btn">
  </div>
  <div class="col">
    <a class="btn btn-block" href="#!">Block btn</a>
  </div>
  <pre class="code prettyprint"><code class="code-content">&lt;!-- Default btn --&gt;
&lt;a class=&quot;btn&quot; href=&quot;#&quot;&gt;Default btn&lt;/a&gt;

&lt;!-- Outline btn --&gt;
&lt;button class=&quot;btn btn-outline&quot;&gt;Outline btn&lt;/button&gt;

&lt;!-- Clear btn --&gt;
&lt;input class=&quot;btn btn-clear&quot; type=&quot;submit&quot; value=&quot;Clear btn&quot;&gt;

&lt;!-- Easily apply the .btn class for btn style in the anchor element. --&gt;</code></pre>
</section>

<section class="container docs-section" id="lists">
  <h1 class="title">Lists</h1>
  <p>The List is a very versatile and common way to display items. Ghast has three types of lists: The unordered list use <code>&lt;ul&gt;</code> element will be marked with a outline circles, the ordered list use <code>&lt;ol&gt;</code> element and your items will be marked with numbers, the description list use <code>&lt;dl&gt;</code> element and your items will not be marking, only spacings.</p>
  <div class="row example">
    <div class="col">
      <ul>
        <li>Unordered list item 1</li>
        <li>Unordered list item 2</li>
      </ul>
    </div>
    <div class="col">
      <ol>
        <li>Ordered list item 1</li>
        <li>Ordered list item 2</li>
      </ol>
    </div>
    <div class="col">
      <dl>
        <dt>Description list item 1</dt>
        <dd>Description list item 1.1</dd>
      </dl>
    </div>
  </div>
  <pre class="code prettyprint"><code class="code-content">&lt;!-- Unordered list --&gt;
&lt;ul&gt;
  &lt;li&gt;Unordered list item 1&lt;/li&gt;
  &lt;li&gt;Unordered list item 2&lt;/li&gt;
&lt;/ul&gt;

&lt;!-- Ordered list --&gt;
&lt;ol&gt;
  &lt;li&gt;Ordered list item 1&lt;/li&gt;
  &lt;li&gt;Ordered list item 2&lt;/li&gt;
&lt;/ol&gt;

&lt;!-- Description list --&gt;
&lt;dl&gt;
  &lt;dt&gt;Description list item 1&lt;/dt&gt;
  &lt;dd&gt;Description list item 1.1&lt;/dd&gt;
&lt;/dl&gt;

&lt;!-- Easily change any &lt;dl&gt;, &lt;ul&gt; or an &lt;ol&gt; to get clear lists, number lists or outline circles. --&gt;</code></pre>
</section>

<section class="container docs-section" id="header">
  <h1 class="title">Header</h1>
  <p>The header is a responsive horizontal navigation component that can contain links, tabs, btns, etc..</p>
  <div class="example">
    <div class="row">
      <div class="col">
        <header class="header">
          <div class="container">
            <div class="header-left">
              <a class="header-item" href="#">
                My Project
              </a>
              <a class="header-tab" href="#">
                Tab
              </a>
              <a class="header-tab is-active" href="#">
                Active tab
              </a>
            </div>
            <span class="header-toggle">
              <span></span>
              <span></span>
              <span></span>
            </span>
            <div class="header-right header-menu">
              <span class="header-item">
                <a href="#">Nav item</a>
              </span>
              <span class="header-item">
                <a href="#">Other nav item</a>
              </span>
              <span class="header-item">
                <a class="btn" href="#">btn</a>
              </span>
            </div>
          </div>
        </header>
      </div>
    </div>
  </div>
  <pre class="code prettyprint"><code class="code-content">&lt;header class=&quot;header&quot;&gt;
  &lt;div class=&quot;container&quot;&gt;
    &lt;!-- Left side --&gt;
    &lt;div class=&quot;header-left&quot;&gt;
      &lt;a class=&quot;header-item&quot; href=&quot;#&quot;&gt;
        &lt;img src=&quot;logo_text.svg&quot; alt=&quot;Logo&quot;&gt;
      &lt;/a&gt;
      &lt;a class=&quot;header-tab&quot; href=&quot;#&quot;&gt;Tab&lt;/a&gt;
      &lt;a class=&quot;header-tab is-active&quot; href=&quot;#&quot;&gt;Active tab&lt;/a&gt;
    &lt;/div&gt;

    &lt;!-- Hamburger menu (on mobile) --&gt;
    &lt;span class=&quot;header-toggle&quot;&gt;
      &lt;span&gt;&lt;/span&gt;
      &lt;span&gt;&lt;/span&gt;
      &lt;span&gt;&lt;/span&gt;
    &lt;/span&gt;

    &lt;!-- Right side --&gt;
    &lt;div class=&quot;header-right header-menu&quot;&gt;
      &lt;span class=&quot;header-item&quot;&gt;
        &lt;a href=&quot;#&quot;&gt;Nav item&lt;/a&gt;
      &lt;/span&gt;
      &lt;span class=&quot;header-item&quot;&gt;
        &lt;a href=&quot;#&quot;&gt;Other nav item&lt;/a&gt;
      &lt;/span&gt;
      &lt;span class=&quot;header-item&quot;&gt;
        &lt;a class=&quot;btn&quot; href=&quot;#&quot;&gt;btn&lt;/a&gt;
      &lt;/span&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;/header&gt;</code></pre>
</section>

<section class="container docs-section" id="footer">
  <h1 class="title">Footer</h1>
  <p>The footer is a simple responsive component which can contain almost anything.</p>
  <div class="example">
    <div class="footer">
      <div class="container">
        <div class="content is-centered">
          <p>
            Made with ♥ in Montreal, Canada by various peeps. Licensed under the <a target="blank" href="https://github.com/voident/ghast#license" title="MIT License">MIT License</a>.
          </p>
        </div>
      </div>
    </div>
  </div>
  <pre class="code prettyprint"><code class="code-content">&lt;footer class=&quot;footer&quot;&gt;
  &lt;div class=&quot;container&quot;&gt;
    &lt;div class=&quot;content is-centered&quot;&gt;
      &lt;p&gt;
        Made with &hearts; in Montreal, Canada by various peeps. Licensed under the &lt;a target=&quot;blank&quot; href=&quot;https://github.com/voident/ghast#license&quot; title=&quot;MIT License&quot;&gt;MIT License&lt;/a&gt;.
      &lt;/p&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;/footer&gt;</code></pre>
</section>

<section class="container docs-section" id="forms">
  <h1 class="title">Forms</h1>
  <p>Forms have never been exactly fun to work with, and they can be downright painful on a mobile device with its on-screen keyboard. Ghast helps to make this much easier with design focused on the user experience.</p>
  <div class="example">
    <form action="javascript: void(0)">
      <fieldset>
        <label for="nameField">Name</label>
        <input type="text" placeholder="Your name" id="nameField">
        <input class="red" type="text" placeholder="Red input">
        <label for="commentField">Comment</label>
        <textarea placeholder="Are you a meme?" id="commentField"></textarea>
        <div class="float-right">
          <input type="checkbox" id="confirmField">
          <label class="label-inline" for="confirmField">Send a copy to yourself</label>
        </div>
        <input class="btn-primary" type="submit" value="Send">
      </fieldset>
    </form>
  </div>
  <pre class="code prettyprint"><code class="code-content">&lt;form&gt;
  &lt;fieldset&gt;
    &lt;label for=&quot;nameField&quot;&gt;Name&lt;/label&gt;
    &lt;input type=&quot;text&quot; placeholder=&quot;Your name&quot; id=&quot;nameField&quot;&gt;
    &lt;input class=&quot;red&quot; type=&quot;text&quot; placeholder=&quot;Red input&quot;&gt;
    &lt;label for=&quot;commentField&quot;&gt;Comment&lt;/label&gt;
    &lt;textarea placeholder=&quot;Are you a meme?&quot; id=&quot;commentField&quot;&gt;&lt;/textarea&gt;
    &lt;div class=&quot;float-right&quot;&gt;
      &lt;input type=&quot;checkbox&quot; id=&quot;confirmField&quot;&gt;
      &lt;label class=&quot;label-inline&quot; for=&quot;confirmField&quot;&gt;Send a copy to yourself&lt;/label&gt;
    &lt;/div&gt;
    &lt;input class=&quot;btn&quot; type=&quot;submit&quot; value=&quot;Send&quot;&gt;
  &lt;/fieldset&gt;
&lt;/form&gt;

&lt;!-- Always wrap checkbox and radio inputs in a label and use a &lt;span class=&quot;label-inline&quot;&gt; inside of it --&gt;</code></pre>
</section>

<section class="container docs-section" id="switches">
  <h1 class="title">Switches</h1>
  <p>Ghast provides beautifully crafted switches as an alternative to checkboxes that convey a more familliar experience to mobile users and overall, they're just pretty!</p>
  <div class="example">
    <label class="switch">
      <input type="checkbox" />
      <i class="switch-icon"></i> Send me emails with news and tips
    </label>
  </div>
  <pre class="code prettyprint"><code class="code-content">&lt;label class=&quot;switch&quot;&gt;
  &lt;input type=&quot;checkbox&quot; /&gt;
  &lt;i class=&quot;switch-icon&quot;&gt;&lt;/i&gt; Send me emails with news and tips
&lt;/label&gt;</code></pre>
</section>

<section class="container docs-section" id="tables">
  <h1 class="title">Tables</h1>
  <p>The Table element represents data in two dimensions or more. We encourage the use of proper formatting with <code>&lt;thead&gt;</code> and <code>&lt;tbody&gt;</code> to create a <code>&lt;table&gt;</code>. The code becomes cleaner without disturbing understanding.</p>
  <div class="example">
    <table class="bordered">
      <thead>
        <tr>
          <th>Name</th>
          <th>Age</th>
          <th>Height</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Stephen Curry</td>
          <td>27</td>
          <td>1,91</td>
          <td>Akron, OH</td>
        </tr>
        <tr>
          <td>Klay Thompson</td>
          <td>25</td>
          <td>2,01</td>
          <td>Los Angeles, CA</td>
        </tr>
      </tbody>
    </table>
  </div>
  <pre class="code prettyprint"><code class="code-content">&lt;table&gt;
  &lt;thead&gt;
    &lt;tr&gt;
      &lt;th&gt;Name&lt;/th&gt;
      &lt;th&gt;Age&lt;/th&gt;
      &lt;th&gt;Height&lt;/th&gt;
      &lt;th&gt;Location&lt;/th&gt;
    &lt;/tr&gt;
  &lt;/thead&gt;
  &lt;tbody&gt;
    &lt;tr&gt;
      &lt;td&gt;Stephen Curry&lt;/td&gt;
      &lt;td&gt;27&lt;/td&gt;
      &lt;td&gt;1,91&lt;/td&gt;
      &lt;td&gt;Akron, OH&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td&gt;Klay Thompson&lt;/td&gt;
      &lt;td&gt;25&lt;/td&gt;
      &lt;td&gt;2,01&lt;/td&gt;
      &lt;td&gt;Los Angeles, CA&lt;/td&gt;
    &lt;/tr&gt;
  &lt;/tbody&gt;
&lt;/table&gt;</code></pre>
</section>

<section class="container docs-section" id="grids">
  <h1 class="title">Grids</h1>
  <p>The default grid is a fluid system with a max width of <code>64.5rem</code> <small>(645px)</small>, that shrinks with the browser/device at smaller sizes. The max width can be changed with one line of CSS and all columns will resize accordingly. Ghast is different than most because of its use of the <strong>CSS Flexible Box Layout Module standard</strong>. The advantage is the simplicity, and we know that a functional code is very important for maintainability and scalability. Simply add the columns you want in a row, and they'll evenly take up the available space. If you want three columns, add three columns, if you want five columns, add five columns. There is no restriction on number of columns, but we advise you to follow a design pattern of grid system. See more tips on best practices in the <a href="#tips" title="Tips">tips</a>.</p>
  <div class="example">
    <div class="container">
      <div class="row">
        <div class="col"><span class="col-demo">.col</span></div>
        <div class="col"><span class="col-demo">.col</span></div>
        <div class="col"><span class="col-demo">.col</span></div>
        <div class="col"><span class="col-demo">.col</span></div>
      </div>
      <div class="row">
        <div class="col"><span class="col-demo">.col</span></div>
        <div class="col col-50 col-o-25"><span class="col-demo">.col-50.col-o-25</span></div>
      </div>
      <div class="row">
        <div class="col"><span class="col-demo">.col</span></div>
      </div>
    </div>
  </div>
  <pre class="code prettyprint"><code class="code-content">&lt;!-- .container is main centered wrapper --&gt;
&lt;div class=&quot;container&quot;&gt;

  &lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;col&quot;&gt;.col&lt;/div&gt;
    &lt;div class=&quot;col&quot;&gt;.col&lt;/div&gt;
    &lt;div class=&quot;col&quot;&gt;.col&lt;/div&gt;
    &lt;div class=&quot;col&quot;&gt;.col&lt;/div&gt;
  &lt;/div&gt;

  &lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;col&quot;&gt;.col&lt;/div&gt;
    &lt;div class=&quot;col col-50 col-offset-25&quot;&gt;.col col-50 col-offset-25&lt;/div&gt;
  &lt;/div&gt;

  &lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;col&quot;&gt;&lt;span class=&quot;col-demo&quot;&gt;.col&lt;/span&gt;&lt;/div&gt;
  &lt;/div&gt;

&lt;/div&gt;

&lt;!-- Every .col added inside a .row will automatically receive an equal amount of the available area. --&gt;</code></pre>
</section>

<section class="container docs-section" id="codes">
  <h1 class="title">Codes</h1>
  <p>The Code element represents a fragment of computer code. Just wrap anything in a <code>&lt;code&gt;</code> and it will appear like this. if you need a block, by default, enter the <code>&lt;code&gt;</code> within the <code>&lt;pre&gt;</code> element.</p>
  <div class="example">
<pre><code>.ghast {
color: #9575CD;
}
</code></pre>
  </div>
  <pre class="code prettyprint"><code class="code-content">&lt;pre&gt;&lt;code&gt;
.ghast {
color: #9575CD;
}
&lt;/code&gt;&lt;/pre&gt;</code></pre>
</section>

<section class="container docs-section" id="utilities">
  <h1 class="title">Utilities</h1>
  <p>Ghast has some functional classes to improve the performance and productivity everyday.</p>
  <pre class="code prettyprint lang-html"><code class="code-content">&lt;!-- Functional Classes --&gt;

&lt;!-- Clear any float  --&gt;
&lt;div class=&quot;clearfix&quot;&gt;

&lt;!-- Float either directions --&gt;
&lt;div class=&quot;float-left&quot;&gt;&lt;/div&gt;
&lt;div class=&quot;float-right&quot;&gt;&lt;/div&gt;

&lt;/div&gt;</code></pre>
</section>

<section class="container docs-section" id="tips">
  <h1 class="title">Tips</h1>
  <p>Tips, techniques, and best practice on using Cascading Style Sheets.</p>
  <p><strong>Mobile First</strong></p>
  <p>The Mobile First is the design strategy that takes priority development for mobile devices like smartphones and tablets. It means all styles outside of a media queries apply to all devices, then larger screens are targeted for enhancement. This prevents small devices from having to parse tons of unused CSS. Ghast use some breakpoints like these:</p>
  <div class="example row">
    <ul>
      <li>Larger than <strong>Mobile</strong> screen: 40.0rem <span class="heading-font-size">(640px)</span></li>
      <li>Larger than <strong>Tablet</strong> screen: 80.0rem <span class="heading-font-size">(1280px)</span></li>
      <li>Larger than <strong>Desktop</strong> screen: 120.0rem <span class="heading-font-size">(1920px)</span></li>
    </ul>
  </div>
  <pre class="code prettyprint lang-css"><code class="code-content">/* Mobile First Media Queries */

/* Base style */
{ ... }

/* Sass media queries  */

/* Larger than mobile screen */
+media(mobile)

/* Larger than tablet screen */
+media(tablet)

/* Larger than desktop screen */
+media(desktop)
</code></pre>
  <p><strong>Extending The Inheritances</strong></p>
  <p>The style of an element can be defined in several different locations, which interact in a complex way. It is form of interaction makes CSS powerful, but it can make it confusing and difficult to debug.</p>
  <div class="example row">
    <div class="col">
      <a href="javascript: void(0)" class="btn">Default .btn</a>
      <a href="javascript: void(0)" class="btn btn-outline">Outline .btn</a>
      <a href="javascript: void(0)" class="btn btn-clear">Clear .btn</a>
    </div>
    <div class="col">
      <a href="javascript: void(0)" class="btn green">.green</a>
      <a href="javascript: void(0)" class="btn green btn-outline">.green</a>
      <a href="javascript: void(0)" class="btn green btn-clear">.green</a>
    </div>
    <div class="col col-20">
      <a href="javascript: void(0)" class="btn btn-sm">.btn-sm</a>
      <a href="javascript: void(0)" class="btn btn-sm btn-outline">.btn-sm</a>
      <a href="javascript: void(0)" class="btn btn-sm btn-clear">.btn-sm</a>
    </div>
    <div class="col">
      <a href="javascript: void(0)" class="btn btn-lg">.btn-lg</a>
      <a href="javascript: void(0)" class="btn btn-lg btn-outline">.btn-lg</a>
      <a href="javascript: void(0)" class="btn btn-lg btn-clear">.btn-lg</a>
    </div>
  </div>
  <pre class="code prettyprint lang-css"><code class="code-content">/* Extending The Inheritances */

/* Applying color variation */
.btn .btn-green {
background-color: #536DFE;
border-color: #536DFE;
}
.btn .btn-green .btn-outline {
color: #536DFE;
}
.btn .btn-green .btn-clear {
color: #536DFE;
}

/* Applying size variation */
.btn .btn-sm {
font-size: .8rem;
height: 2.8rem;
line-height: 2.8rem;
padding: 0 1.5rem;
}

.btn .btn-lg {
font-size: 1.4rem;
height: 4.5rem;
line-height: 4.5rem;
padding: 0 2rem;
}</code></pre>
</section>

<section class="container docs-section" id="browser-support">
  <h1 class="title">Browser Support</h1>
  <p>While not designed for old browsers, Ghast has support for some old versions, but we recommend using the latest versions of their browsers.</p>
  <ul>
    <li>Chrome <small>latest</small></li>
    <li>Firefox <small>latest</small></li>
    <li>IE <small>latest</small></li>
    <li>Opera <small>latest</small></li>
    <li>Safari <small>latest</small></li>
  </ul>
</section>

<section class="container docs-section" id="examples">
  <h1 class="title">Examples</h1>
  <p>You can view more examples of using Ghast.</p>
  <p>
    <ul>
      <li><a href="{{ site.url }}/index.html#getting-started" title="Getting Started">Getting Started</a></li>
      <li><a href="{{ site.url }}/index.html#typography" title="Typography">Typography</a></li>
      <li><a href="{{ site.url }}/index.html#blockquotes" title="Blockquotes">Blockquotes</a></li>
      <li><a href="{{ site.url }}/index.html#buttons" title="btns">Buttons</a></li>
      <li><a href="{{ site.url }}/index.html#lists" title="Lists">Lists</a></li>
      <li><a href="{{ site.url }}/index.html#header" title="Header">Header</a></li>
      <li><a href="{{ site.url }}/index.html#footer" title="Footer">Footer</a></li>
      <li><a href="{{ site.url }}/index.html#forms" title="Forms">Forms</a></li>
      <li><a href="{{ site.url }}/index.html#switches" title="Switches">Switches</a></li>
      <li><a href="{{ site.url }}/index.html#tables" title="Tables">Tables</a></li>
      <li><a href="{{ site.url }}/index.html#grids" title="Grids">Grids</a></li>
      <li><a href="{{ site.url }}/index.html#codes" title="Codes">Codes</a></li>
      <li><a href="{{ site.url }}/index.html#utilities" title="Utilities">Utilities</a></li>
      <li><a href="{{ site.url }}/index.html#tips" title="Tips">Tips</a></li>
      <li><a href="{{ site.url }}/index.html#browser-support" title="Browser Support">Browser Support</a></li>
    </ul>
  </p>
</section>

<section class="container docs-section" id="contributing">
  <h1 class="title">Contributing</h1>
  <p>Want to contribute? Follow these <a href="https://github.com/impria/ghast/blob/master/contributing.md" title="Contributing">recommendations</a>.</p>
</section>


<section class="container docs-section">
  <div class="row">
    <div class="col">
      <h2>Writer's Desk</h2>
      <ul class="post-list">

          <li>
            <span class="post-meta">Aug 6, 2016</span>

            <h4>
              <a href="/jekyll/update/2016/08/06/welcome-to-jekyll.html">Welcome to Jekyll!</a>
            </h4>
            <p>
              <p>You’ll find this post in your <code class="highlighter-rouge">_posts</code> directory. Go ahead and edit it and re-build the site to see your changes. You can rebuild the site in many different ways, but the most common way is to run <code class="highlighter-rouge">jekyll serve</code>, which launches a web server and auto-regenerates your site when a file is updated.</p>


            </p>
          </li>

      </ul>
      <p class="text-center">
        <a href="/feed.xml" class="btn">Subscribe via RSS</a>
      </p>
    </div>
  </div>
</section>
